/**
 * 
 */
package com.experian.assignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author AJAY
 *
 */
public class GeneralUtility {

	static final Logger LOG = Logger.getLogger(GeneralUtility.class);
	public static Date convertStringToDate(String dateFormat, String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			return sdf.parse(date);
		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

}
