<html ng-app="assignmentApp">
<head>
<title></title>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
<script type="text/javascript"
	src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-route.js"></script>
<script type="text/javascript"
	src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-resource.js"></script>
<script type="text/javascript" src="js/assignmentApp.js"></script>
<style type="text/css">
body {
	padding: 0 2em;
	font-family: Montserrat, sans-serif;
	-webkit-font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
	color: #444;
	background: #eee;
}

span {
	color: red;
}

Adding rules for fieldset and legend:


fieldset {
	background: #f2f2e6;
	padding: 10px;
	border: 1px solid #fff;
	border-color: #fff #666661 #666661 #fff;
	margin-bottom: 36px;
	width: 600px;
}

legend {
	background: #bfbf30;
	color: #fff;
	font: 17px/21px Calibri, Arial, Helvetica, sans-serif;
	padding: 0 10px;
	margin: -26px 0 0 -11px;
	font-weight: bold;
	border: 1px solid #fff;
	border-color: #e5e5c3 #505014 #505014 #e5e5c3;
}

.report-table {
	background: #34495E;
	color: #fff;
	border-radius: .4em;
	overflow: hidden;
	width: 80%;
	tr
	{
	border-color
	:
	lighten(
	#34495E
	,
	10%
	);
}

}
form {
	width: 80%
}

div {
	height: 30px;
}

#display-btn {
	background: #B9DFFF;
	color: #fff;
	border: 1px solid #eee;
	border-radius: 20px;
	box-shadow: 5px 5px 5px #eee;
}

#display-btn:hover {
	background: #016ABC;
	color: #fff;
	border: 1px solid #eee;
	border-radius: 20px;
	box-shadow: 5px 5px 5px #eee;
}
</style>

</head>
<body>
	<form class="form-horizontal" role="form"
		ng-submit="getWebsiteReport()" ng-controller="reportCntrl">
		<fieldset>
			<legend>Website Report</legend>

			<div class="form-group" style="margin-left: 350px;">
				<div class="input-group date">
					<label for="visitDate"
						style="font-family: serif; color: purple; font-size: 18; font-style: oblique;">
						VisitDate</label> <span>*</span> <input type="date" class="form-control"
						id="visitDate" ng-model="visitDate" required="required"
						style="padding-right: .5em; border-bottom-style: outset;">
				</div>
			</div>

			<div class="form-group" style="margin-left: 450px;">
				<button type="submit" id="display-btn" style="border: groove;">Get
					Top Websites</button>
			</div>
			<div class="form-group" style="margin-left: 145px;">
				<table class="report-table">
					<tr>
						<th style="color: orange;">Website</th>
						<th style="color: orange;">No.Of Visits</th>
						<th style="color: orange;">Visit Date</th>
					</tr>
					<tr ng-repeat="report in reports">
						<td style="text-align: center;">{{report.websiteName}}</td>
						<td style="text-align: center;">{{report.totalVisits}}</td>
						<td style="text-align: center;">{{report.visitDate}}</td>
					</tr>
				</table>
			</div>
		</fieldset>
	</form>
</body>
</html>
