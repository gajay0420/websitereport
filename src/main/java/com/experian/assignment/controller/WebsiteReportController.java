/**
 * 
 */
package com.experian.assignment.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.experian.assignment.WebsiteReport;
import com.experian.assignment.service.WebsiteDisplayServiceSpec;

/**
 * @author AJAY
 *
 */
@Controller
public class WebsiteReportController {

	@Autowired
	private WebsiteDisplayServiceSpec websiteReportService;

	private static final Logger LOG = Logger.getLogger(WebsiteReportController.class);

	@RequestMapping(value = "/getReport", produces = "application/json")
	@ResponseBody
	public final List<WebsiteReport> getReport(@RequestParam("visitDate") String visitDate) {
		websiteReportService.saveReports();

		if (LOG.isInfoEnabled())
			LOG.info("Request for get website reports on:" + visitDate);
		List<WebsiteReport> reports = new ArrayList<WebsiteReport>();
		try {
			if (visitDate != null && !visitDate.isEmpty()) {
				reports = websiteReportService.findReportsByDate(visitDate);
				if (LOG.isInfoEnabled())
					LOG.info("No.of Top Websites:" + reports.size());
			}
		} catch (Exception e) {
			if (LOG.isInfoEnabled())
				LOG.info(e.getMessage(), e);
		}
		return reports;
	}
}
