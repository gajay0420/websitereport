/**
 * 
 */
package com.experian.assignment.daoSpec;

import java.util.Date;
import java.util.List;

import com.experian.assignment.WebsiteReport;

/**
 * @author AJAY
 *
 */
public interface WebsiteDisplayDaoSpec {

	/**
	 * @param date
	 *            <p>
	 *            -- Visit Date for filtering the data
	 *            </p>
	 * @return <p>
	 *         -- Returns No.of web site names if exist on filtered date or else empty arrayList.
	 *         </p>
	 */
	public List<WebsiteReport> findTopWebsitesByDate(Date date);

	void saveReport();

}
