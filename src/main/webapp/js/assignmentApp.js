var assignmentApp = angular.module("assignmentApp", [ 'ngRoute' ]);

assignmentApp.controller('reportCntrl', [ '$scope', '$http',
		function($scope, $http) {
			var reports = [];
			$scope.getWebsiteReport = function() {
				$http({
					method : 'get',
					url : "http://localhost:8080/websitereport/getReport",
					params : {
						visitDate : $scope.visitDate
					},
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						'Access-Control-Allow-Credentials' : true
					}
				}).success(function(data, status, headers, config) {
					if(data.length == 0){
						alert("No website is viewed on that date:"+ $scope.visitDate+ " plese choose another date " );
					}
					$scope.reports = data;
				}).error(function(data, status, headers, config) {
					alert("Something went wrong please try again.......");
				});
				$scope.visitDate = '';
			};
		} ]);
