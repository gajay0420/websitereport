/**
 * 
 */
package com.experian.assignment.daoimpl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.experian.assignment.GeneralUtility;
import com.experian.assignment.WebsiteReport;
import com.experian.assignment.daoSpec.WebsiteDisplayDaoSpec;

/**
 * @author AJAY
 *
 */
@Repository
@Transactional
public class WebsiteDisplayDao implements WebsiteDisplayDaoSpec {

	private SessionFactory sessionFactory;
	private static Logger LOG = Logger.getLogger(WebsiteDisplayDao.class);

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WebsiteReport> findTopWebsitesByDate(Date date) {

		List<WebsiteReport> reports = null;

		String hql = "from com.experian.assignment.WebsiteReport a where a.visitDate = :date "
				+ " order by totalVisits desc";
		reports = sessionFactory.getCurrentSession().createQuery(hql).setDate("date", date).setMaxResults(5).list();

		if (reports != null && !reports.isEmpty())
			return reports;
		return new ArrayList<WebsiteReport>();
	}

	@Override
	public void saveReport() {

		String csvFile = "C:/Users/AJAY/Desktop/pdilabs/Input/data.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\\|";

		try {

			br = new BufferedReader(new FileReader(csvFile));
			int iteration = 0;
			while ((line = br.readLine()) != null) {

				if (iteration == 0) {
					iteration++;
					continue;
				}
				String[] reports = line.split(cvsSplitBy);

				System.out.println("date :" + reports[0] + " - website:" + reports[1] + " - visits:" + reports[2]);

				WebsiteReport websiteReport = new WebsiteReport();
				websiteReport.setVisitDate(GeneralUtility.convertStringToDate("yyyy-MM-dd", reports[0]));
				websiteReport.setWebsiteName(reports[1]);
				websiteReport.setTotalVisits(Integer.parseInt(reports[2]));
				sessionFactory.getCurrentSession().save(websiteReport);
				if (LOG.isInfoEnabled())
					LOG.info("WebsiteReport object saved in database successfully with id:" + websiteReport.getId());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
