/**
 * 
 */
package com.experian.assignment.serviceimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.experian.assignment.GeneralUtility;
import com.experian.assignment.WebsiteReport;
import com.experian.assignment.daoSpec.WebsiteDisplayDaoSpec;
import com.experian.assignment.service.WebsiteDisplayServiceSpec;

/**
 * @author AJAY
 *
 */
@Transactional
@Service
public class WebsiteDisplayService implements WebsiteDisplayServiceSpec {

	@Autowired
	private WebsiteDisplayDaoSpec websiteDisplayDaoSpec;

	private String dateFormat = "yyyy-MM-dd";

	@Override
	public List<WebsiteReport> findReportsByDate(String date) {
		Date visitDate = GeneralUtility.convertStringToDate(dateFormat, date);
		return websiteDisplayDaoSpec.findTopWebsitesByDate(visitDate);
	}

	@Override
	public void saveReports() {
		websiteDisplayDaoSpec.saveReport();
	}

}
