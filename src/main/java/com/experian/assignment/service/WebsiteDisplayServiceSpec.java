/**
 * 
 */
package com.experian.assignment.service;

import java.util.List;

import com.experian.assignment.WebsiteReport;

/**
 * @author AJAY
 *
 */
public interface WebsiteDisplayServiceSpec {

	/**
	 * @param date
	 *            <p>
	 *            -- Visit Date for filtering the data
	 *            </p>
	 * @return <p>
	 *         -- Returns No.of web site names if exist on filtered date or else empty arrayList.
	 *         </p>
	 */
	public List<WebsiteReport> findReportsByDate(String date);

	/**
	 * -- save csv file data into database
	 */
	public void saveReports();

}
