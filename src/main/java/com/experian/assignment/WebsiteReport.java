/**
 * 
 */
package com.experian.assignment;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author AJAY
 *
 */
@Table(name = "WEBSITE_REPORT")
@Entity
public class WebsiteReport {

	@GeneratedValue(generator = "idGen")
	@GenericGenerator(strategy = "uuid.hex", name = "idGen")
	@Id
	@Column(name = "ID", length = 50)
	private String id;

	@Column(name = "WEBSITE_NAME", length = 255)
	private String websiteName;

	@Column(name = "TOTAL_VISITS")
	private long totalVisits;

	@Temporal(TemporalType.DATE)
	@Column(name = "VISIT_DATE")
	private Date visitDate;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the websiteName
	 */
	public String getWebsiteName() {
		return websiteName;
	}

	/**
	 * @param websiteName
	 *            the websiteName to set
	 */
	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

	/**
	 * @return the totalVisits
	 */
	public long getTotalVisits() {
		return totalVisits;
	}

	/**
	 * @param totalVisits
	 *            the totalVisits to set
	 */
	public void setTotalVisits(long totalVisits) {
		this.totalVisits = totalVisits;
	}

	/**
	 * @return the visitDate
	 */
	public Date getVisitDate() {
		return visitDate;
	}

	/**
	 * @param visitDate
	 *            the visitDate to set
	 */
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WebsiteReport [ websiteName=" + websiteName + ", totalVisits=" + totalVisits + ", visitDate="
				+ visitDate + "]";
	}

}
